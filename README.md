Java version:
```
tedi@allcome:/opt/java/bin$ ./java -version  
java version "1.8.0_201"  
Java(TM) SE Runtime Environment (build 1.8.0_201-b09)  
Java HotSpot(TM) 64-Bit Server VM (build 25.201-b09, mixed mode)  
tedi@allcome:/opt/java/bin$  
```

Clone this repository:
```
cd ~/
git clone git@bitbucket.org:suryacom/java.git
sudo mv java /opt
cd /opt
chown $USER:$GROUP -R /opt/java
```
Connect your existing repository to Bitbucket:
```
cd /opt/java/
git remote add origin git@bitbucket.org:suryacom/java.git
git add -A
git commit -am "first commit"
git push -u origin master
```
